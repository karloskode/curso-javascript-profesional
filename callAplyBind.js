function executor(funcion){
    funcion.call(tutor);
}

let tutor = {
    nombre: "Carlos",
    apellido: "Encalada",
    nombreCompleto: function(){
        console.log(this.nombre + " " + this.apellido);
    }
}
executor(tutor.nombreCompleto);

//Aply tiene la misma funcion pero los parametros van en un arreglo
function saluda(nombre){console.log("Hola " + nombre);}
saluda.call(window, "Carlos");
saluda.apply(window, ["PEPE"]);

//bind permite 'setear' el contexto, asi no se pierde el dinamismo de executor
function executorB(funcion){
    funcion();
}
tutor.nombre = "Carlos G.";
executorB(tutor.nombreCompleto.bind(tutor));