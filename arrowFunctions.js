let demo = ()=>{
    console.log("Hola mundo");
}

demo();

//arrow function anonima
()=>{console.log("que mas")};

//omitir llaves
let saludar = ()=> console.log("Hola dev");
saludar();

//alternativa a return
function suma(a, b){
    return a + b;
}
sumar = (a, b) => a + b;

console.log(sumar(2,3));

//no modificar el contexto
let tutor = {
    nombre: "Carlos",
    apellido: "Encalada",
    nombreCompleto: function(){
        setTimeout(()=>{
            console.log(this.nombre + " " + this.apellido)}, 1000);
        }
    }
tutor.nombreCompleto();