Los siguientes, son una serie de ejercicios para que practiques los  conocimientos adquiridos después del bloque de conceptos básicos.

1. Tomando como entrada un número entero, imprimir si es par o impar.
2. Recibir dos números usando prompt y sumarlos, restarlos, dividirlos y multiplicarlos
3. Imprimir la sucesión fibonacci el número de veces que indicó el usuario
4. Programar el juego del "Número Mágico" en el que se define un  número y el usuario trata de adivinarlo, si el número que ingresó el  usuario es menor, imprimir la pista "El número mágico es mayor", si el  número que ingresó el usuario es mayor, imprimir la pista "El número  mágico es menor",