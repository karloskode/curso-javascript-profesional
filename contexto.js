//Cuando la funcion pertenece a otro objeto, this devuelve el objeto

let objeto = {
    demo: function(){
        console.log(this);
    }
}
objeto.demo();

//Funcion que tiene como elemento un funcion
let executor ={
    execute: function(f){f();}
}
executor.execute(objeto.demo);

//Guardar como propiedad de otro objecto la Funcion que le pertenece a un objeto distinto
let ejecutor = {
    funcion: null,
    execute: function(f){
        this.funcion = f;
        this.funcion();
    }
}
ejecutor.execute(objeto.demo);

console.log("Cambio no deseado de this")
let usuario = {
    nombre: "Carlos",
    apellido: "Encalada",
    nombreCompleto: function(){
        console.log(this.nombre + " " + this.apellido);
    }
}
usuario.nombreCompleto();
ejecutor.execute(usuario.nombreCompleto);