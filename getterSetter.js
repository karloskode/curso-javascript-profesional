class Usuario{
    constructor(nombre){
        this._name = nombre;
    }

    get name(){
        return this._name.charAt(0).toUpperCase() + this._name.slice(1);
    }

    set name(nombre){
        this._name = nombre;
    }
}

let user = new Usuario("SK");
console.log(user.name);

user.name = "Carlos";
console.log(user.name);