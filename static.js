class Usuario{
    constructor(permisos = "lectura"){
        this._permisos = permisos;
    }

    static createAdmin(){
        let user = new Usuario("readWrite");
        return user;
    }
}

let usuario = Usuario.createAdmin();
console.log(usuario._permisos);